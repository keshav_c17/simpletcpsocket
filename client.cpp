#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>

int main(int argc, char *argv[])
{

    int socket_fd, serverPort;
    struct sockaddr_in serverAddr;      // part of netinet/in.h
    struct hostent *serverIP;           // part of netdb.h
    char buffer[256];

    if (argc < 3) {
       fprintf(stderr,"Usage: %s <hostname/serverIP> <port>\n", argv[0]);
       exit(0);
    }

    serverPort = atoi(argv[2]);

    socket_fd = socket(AF_INET, SOCK_STREAM, 0);

    if (socket_fd < 0) {
        perror("ERROR opening socket");   
        exit(1);
    }

    serverIP = gethostbyname(argv[1]);
    if (serverIP == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }

    bzero((char *) &serverAddr, sizeof(serverAddr));

    serverAddr.sin_family = PF_INET;
    bcopy((char *)serverIP->h_addr, 
         (char *)&serverAddr.sin_addr.s_addr,
         serverIP->h_length);
    serverAddr.sin_port = htons(serverPort);

    if (connect(socket_fd, (struct sockaddr *) &serverAddr, sizeof(serverAddr)) < 0) {
        perror("ERROR Connecting");   
        exit(1);
    }


    printf("Please enter the message: ");
    
    bzero(buffer,256);
    fgets(buffer,255,stdin);
    if (write(socket_fd, buffer, strlen(buffer)) < 0) {
        perror("ERROR writing to socket");
        exit(1);
    }

    bzero(buffer,256);
    if (read(socket_fd, buffer, 255) < 0) {
        perror("ERROR reading from socket");
        exit(1);
    }
    
    printf("%s\n", buffer);
    
    close(socket_fd);
    
    return 0;

}