#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>


int main(int argc, char *argv[])
{   
    // Declaring some variables/constants/structs
    int serverSocket, clientSocket, portno;
    socklen_t client_len;                           // part of unistd.h
    struct sockaddr_in server_addr, client_addr;    // part of netinet/in.h
    char buffer[256];

    if (argc < 2) {
        fprintf(stderr,"ERROR, no port provided\n");
        exit(1);
    }

    // Creating new socket for server, returns a socket file descriptor or error -1
    serverSocket = socket(AF_INET, SOCK_STREAM, 0);
    if (serverSocket < 0){ 
        perror("ERROR opening socket");  // stdio.h
        exit(1);
    }

    // clear address structure
    bzero((char *) &server_addr, sizeof(server_addr));

    portno = atoi(argv[1]);

    /* setup the host_addr structure for use in bind call */
    server_addr.sin_family = AF_INET;           // Server byte order
    server_addr.sin_addr.s_addr = INADDR_ANY;   // Automatically be filled with current host's IP
    server_addr.sin_port = htons(portno);       // convert short integer value for port must be converted into network byte order

    /*
    *  bind() passes socket file descriptor, the address structure and the length of 
    *  the address structure. This bind() call will bind  the socket to the 
    *  current IP address on port, portno
    */
    if (bind(serverSocket, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0) {
        perror("ERROR on binding");
        exit(1);
    }
    
    /* This listen() call tells the socket to listen to the incoming connections.
    *  The listen() function places all incoming connection into a backlog queue
    *  until accept() call accepts the connection.
    *  Here, we set the maximum size for the backlog queue to 3.
    */
    listen(serverSocket, 3);

    std::cout <<  "Waiting for connections...\n"
              <<  "Server Port:" << portno << std::endl;

    
    /* This accept() function will write the connecting client's address info 
    * into the the address structure and the size of that structure is client_len.
    * The accept() returns a new socket file descriptor for the accepted connection.
    * So, the original socket file descriptor can continue to be used 
    * for accepting new connections while the new socket file descriptor is used for
    * communicating with the connected client.
    */
    client_len = sizeof(client_addr);
    clientSocket = accept(serverSocket, (struct sockaddr *) &client_addr, &client_len);
    if (clientSocket < 0) {
        perror("ERROR on accept");
        exit(1);
    }

    // This send() function sends the 13 bytes of the string to the client socket
    send(clientSocket, "Hello, World!\n", 13, 0);

    bzero(buffer, 256);
    

    int n = read(clientSocket,buffer,255);
    if (n < 0) {
        perror("ERROR reading from socket");
        exit(1);
    }

    printf("Here is the message: %s\n",buffer);

    close(clientSocket);
    close(serverSocket);


    return 0;
}